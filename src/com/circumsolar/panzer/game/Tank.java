package com.circumsolar.panzer.game;

import com.circumsolar.panzer.util.AnimatedSprite;

import java.awt.*;

public class Tank extends AnimatedSprite {

    public Color color;

    public int moveLeftKey;
    public int moveRightKey;
    public int rotateLeftKey;
    public int rotateRightKey;
    public int increasePowerKey;
    public int decreasePowerKey;
    public int fireKey;

    public float velX;

    public float fireAngle;
    public float firePower;

    public boolean fired;
}
