package com.circumsolar.panzer.game;

import com.circumsolar.panzer.util.GamePanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

public class GameModel extends GamePanel {

    private static final int B_WIDTH = 1024;
    private static final int B_HEIGHT = 768;

    private static final int LANDSCAPE_MIN_HEIGHT = 80;
    private static final int LANDSCAPE_MAX_HEIGHT = 200;
    private static final int LANDSCAPE_PEAK_NUM = 5;

    private static final int TANK_LEN = 30;
    private static final int TANK_MAX_VEL = 2;
    private static final int NUM_LEAVES = 200;
    private static final int BULLET_LOAD = 60;

    private static final int TANK_SPRITE_WIDTH = 40;
    private static final int TANK_SPRITE_HEIGHT = 40;
    private static final int TANK_SPRITE_COLUMNS = 5;
    private static final int TANK_TIME_PER_FRAME = 50;
    private static final int TANK_FRAMES = 20;

    private static final Random random = new Random();

    public World world;
    private java.util.List<Tank> tanks;
    private java.util.List<Bullet> bullets;
    private java.util.List<Bullet> leaves;
    private java.util.List<Bullet> particles;

    public int[] landscape;

    public GameModel() {
        super(B_WIDTH, B_HEIGHT);

        loadTexture("mammuth");
        loadTexture("redtank");
        loadTexture("lufa");

        newGame();
    }

    private void newGame() {
        world = new World();
        tanks = new ArrayList<>();
        bullets = new ArrayList<>();
        leaves = new ArrayList<>();
        particles = new ArrayList<>();

        generateLandscape();

        Tank tankA = new Tank();
        tankA.fireAngle = 45;
        tankA.firePower = 200;
        tankA.color = Color.RED;
        tankA.moveLeftKey = KeyEvent.VK_A;
        tankA.moveRightKey = KeyEvent.VK_D;
        tankA.rotateLeftKey = KeyEvent.VK_Q;
        tankA.rotateRightKey = KeyEvent.VK_E;
        tankA.increasePowerKey = KeyEvent.VK_W;
        tankA.decreasePowerKey = KeyEvent.VK_S;
        tankA.fireKey = KeyEvent.VK_SPACE;
        tankA.x = B_WIDTH / 4;

        tankA.width = TANK_SPRITE_WIDTH;
        tankA.height = TANK_SPRITE_HEIGHT;
        tankA.texture = getTexture("redtank");
        tankA.frameCount = TANK_FRAMES;
        tankA.textureColumns = TANK_SPRITE_COLUMNS;
        tankA.timePerFrame = TANK_TIME_PER_FRAME;
        tankA.offsetY = -20;
        tankA.prepare();

        tanks.add(tankA);

        Tank tankB = new Tank();
        tankB.fireAngle = 135;
        tankB.firePower = 200;
        tankB.color = Color.BLUE;
        tankB.moveLeftKey = KeyEvent.VK_NUMPAD4;
        tankB.moveRightKey = KeyEvent.VK_NUMPAD6;
        tankB.rotateLeftKey = KeyEvent.VK_NUMPAD7;
        tankB.rotateRightKey = KeyEvent.VK_NUMPAD9;
        tankB.increasePowerKey = KeyEvent.VK_NUMPAD8;
        tankB.decreasePowerKey = KeyEvent.VK_NUMPAD5;
        tankB.fireKey = KeyEvent.VK_NUMPAD0;
        tankB.x = B_WIDTH - B_WIDTH / 4;

        tankB.width = TANK_SPRITE_WIDTH;
        tankB.height = TANK_SPRITE_HEIGHT;
        tankB.texture = getTexture("redtank");
        tankB.frameCount = TANK_FRAMES;
        tankB.textureColumns = TANK_SPRITE_COLUMNS;
        tankB.timePerFrame = TANK_TIME_PER_FRAME;
        tankB.offsetY = -20;
        tankB.prepare();

        tanks.add(tankB);
    }

    private void generateLandscape() {
        landscape = new int[B_WIDTH];

        int peak = B_HEIGHT - random.nextInt(LANDSCAPE_MAX_HEIGHT - LANDSCAPE_MIN_HEIGHT) - LANDSCAPE_MIN_HEIGHT;
        for (int i = 0; i < LANDSCAPE_PEAK_NUM; i++) {
            int nextPeak = B_HEIGHT - random.nextInt(LANDSCAPE_MAX_HEIGHT - LANDSCAPE_MIN_HEIGHT) - LANDSCAPE_MIN_HEIGHT;
            float incr = (nextPeak - peak) / (float) (B_WIDTH / LANDSCAPE_PEAK_NUM);
            for (int j = 0; j < B_WIDTH / LANDSCAPE_PEAK_NUM; j++) {
                landscape[i * B_WIDTH / LANDSCAPE_PEAK_NUM + j] = (int) (peak + j * incr);
            }
            peak = nextPeak;
        }

        for (int i = 1; i < B_WIDTH; i++) {
            if (landscape[i] == 0) {
                landscape[i] = landscape[i - 1];
            }
        }
    }

    @Override
    protected void update(float delta) {
//        if (getKeyState(INC_GRAVITY)) {
//            world.setGravityForce(world.getGravityForce() + val);
//        }
//        if (getKeyState(DEC_GRAVITY)) {
//            world.setGravityForce(world.getGravityForce() - val);
//        }
//
//        if (getKeyState(INC_WIND_FORCE)) {
//            world.setWindForce(world.getWindForce() + val);
//        }
//        if (getKeyState(DEC_WIND_FORCE)) {
//            world.setWindForce(world.getWindForce() - val);
//        }
//        if (getKeyState(INC_WIND_ANGLE)) {
//            world.setWindAngle(world.getWindAngle() + val);
//        }
//        if (getKeyState(DEC_WIND_ANGLE)) {
//            world.setWindAngle(world.getWindAngle() - val);
//        }

        if (getKeyStateOnce(KeyEvent.VK_ESCAPE)) {
            newGame();
        }

        java.util.List<Bullet> shouldRemove = new ArrayList<>();

        for (Bullet bullet : bullets) {
            bullet.update(delta);
            if (checkBulletCollision(bullet)) {
                shouldRemove.add(bullet);
            }
        }

        for (Bullet bullet : shouldRemove) {
            bullets.remove(bullet);
        }

        shouldRemove.clear();

        for (Bullet bullet : leaves) {
            bullet.update(delta);
            if (checkLeafCollision(bullet)) {
                shouldRemove.add(bullet);
            }
        }

        for (Bullet bullet : shouldRemove) {
            leaves.remove(bullet);
        }

        shouldRemove.clear();

        for (Bullet bullet : particles) {
            bullet.update(delta);
            if (bullet.getX() < 0 || bullet.getX() >= B_WIDTH || bullet.getY() > landscape[(int) bullet.getX()]) {
                shouldRemove.add(bullet);
            }
        }

        for (Bullet bullet : shouldRemove) {
            particles.remove(bullet);
        }

        shouldRemove.clear();

        for (int i = leaves.size(); i < NUM_LEAVES; i++) {
            leaves.add(new Bullet(world, 0.5f, random.nextInt(B_WIDTH), random.nextInt(B_HEIGHT), 0, 0));
        }

        for (Tank tank : tanks) {
            updateTank(delta, tank);
        }
    }

    private boolean checkBulletCollision(Bullet bullet) {
        if (bullet.getX() < 0 || bullet.getX() > B_WIDTH) {
            return bullet.getY() > B_HEIGHT;
        }

        int x = (int) bullet.getX();

        if (bullet.getY() > landscape[x]) {
            for (int i = 0; i < BULLET_LOAD; i++) {
                growLandscape(x);
            }
            return true;
        }

        return false;
    }

    private boolean checkLeafCollision(Bullet bullet) {
        if (bullet.getX() < 0 || bullet.getX() > B_WIDTH) {
            return true;
        }

        int x = (int) bullet.getX();

        if (bullet.getY() > landscape[x]) {
            growLandscape(x);
            return true;
        }

        return false;
    }

    public void growLandscape(int x) {
        boolean left = false, right = false;
        if (x > 0 && (landscape[x - 1] - landscape[x]) > 0) {
            left = true;
        }
        if (x < B_WIDTH - 1 && (landscape[x + 1] - landscape[x]) > 0) {
            right = true;
        }

        if (left && !right) {
            growLandscape(x - 1);
        } else if (!left && right) {
            growLandscape(x + 1);
        } else if (left) {
            if (random.nextBoolean()) {
                growLandscape(x - 1);
            } else {
                growLandscape(x + 1);
            }
        } else {
            landscape[x]--;
        }
    }

    public void digLandscape(int x) {
        if (x < 0 || x >= B_WIDTH) {
            return;
        }

        if (landscape[x] == B_HEIGHT) {
            return;
        }

        boolean left = false, right = false;
        if (x > 0 && (landscape[x - 1] - landscape[x]) < 0) {
            left = true;
        }
        if (x < B_WIDTH - 1 && (landscape[x + 1] - landscape[x]) < 0) {
            right = true;
        }

        if (left && !right) {
            digLandscape(x - 1);
        } else if (!left && right) {
            digLandscape(x + 1);
        } else if (left) {
            if (random.nextBoolean()) {
                digLandscape(x - 1);
            } else {
                digLandscape(x + 1);
            }
        } else {
            landscape[x]++;
        }
    }

    public void updateTank(float delta, Tank tank) {
        float val = 100 * delta;

        if (getKeyState(tank.increasePowerKey)) {
            tank.firePower += val;
        }
        if (getKeyState(tank.decreasePowerKey)) {
            tank.firePower -= val;
        }
        if (getKeyState(tank.rotateLeftKey)) {
            tank.fireAngle += val;
        }
        if (getKeyState(tank.rotateRightKey)) {
            tank.fireAngle -= val;
        }

        if (Math.abs(tank.velX) < TANK_MAX_VEL) {
            if (getKeyState(tank.moveLeftKey)) {
                tank.velX -= delta;
            }
            if (getKeyState(tank.moveRightKey)) {
                tank.velX += delta;
            }
        }

        if (tank.velX < 0) {
            tank.velX += delta / 2;
        }
        if (tank.velX > 0) {
            tank.velX -= delta / 2;
        }

        tank.x += tank.velX;

        if (tank.x < TANK_LEN / 2) {
            tank.velX = 0;
            tank.x = TANK_LEN / 2;
        } else if (tank.x > B_WIDTH - TANK_LEN / 2) {
            tank.velX = 0;
            tank.x = B_WIDTH - TANK_LEN / 2;
        }

        if (tank.fired) {

            if (!getKeyState(tank.fireKey)) {
                tank.fired = false;
            }

        } else if (getKeyState(tank.fireKey)) {

            float startX = (float) (tank.x + 30 * Math.cos(Math.toRadians(tank.fireAngle + 4)));
            float startY = (float) (tank.y - 30 * Math.sin(Math.toRadians(tank.fireAngle + 4))) - 15;

            bullets.add(new Bullet(world, 1, startX, startY, tank.fireAngle, tank.firePower));

            for (int i = 0; i < 10; i++) {
                particles.add(new Bullet(world, 0.5f, startX, startY, tank.fireAngle - 45 + random.nextInt(90), random.nextInt(50) + 20));
            }

            for (int i = 0; i < TANK_LEN * 2; i++) {
                digLandscape((int) tank.x + i - TANK_LEN);
            }

            tank.fired = true;
        }

        Point min = new Point(0, Integer.MAX_VALUE);
        Point max = new Point(0, 0);

        for (int i = 0; i < TANK_LEN; i++) {
            int x = (int) tank.x + i - TANK_LEN / 2;
            int y = landscape[x];

            if (min.y > y) {
                min.x = x;
                min.y = y;
            }
            if (max.y < y) {
                max.x = x;
                max.y = y;
            }
        }

        tank.y = (min.y + max.y) / 2;
        float newAngle = (float) Math.atan2(min.y - max.y, min.x > max.x ? TANK_LEN : -TANK_LEN);

        if (Math.toDegrees(newAngle) > 90 || Math.toDegrees(newAngle) < -90) {
            newAngle = (float) Math.toRadians(Math.toDegrees(newAngle) + 180);
        }

        tank.rotation = newAngle;

        tank.update(delta);

        if (random.nextFloat() < Math.abs(tank.velX) / TANK_MAX_VEL / 4) {
            float x;
            float angle;
            if (tank.velX > 0) {
                x = tank.x - 15;
                angle = 180 - random.nextInt(45);
            } else {
                x = tank.x + 15;
                angle = random.nextInt(45);
            }
            particles.add(new Bullet(world, 0.5f, x, tank.y, angle, 50));
        }

        //System.out.println(tank.color + " " + Math.toDegrees(tank.rotation));
    }

    @Override
    protected void draw(Graphics2D g) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, B_WIDTH, B_HEIGHT);

        g.setColor(Color.WHITE);

        g.drawString(String.format("GRAVITY: %.2f  WIND ANGLE %.2f  WIND FORCE: %.2f",
                world.getGravityForce(), world.getWindAngle(), world.getWindForce()), 10, 15);
        g.drawString(String.format("RED FIRE ANGLE: %.2f  FIRE FORCE: %.2f",
                tanks.get(0).fireAngle, tanks.get(0).firePower), 10, 30);
        g.drawString(String.format("BLUE FIRE ANGLE: %.2f  FIRE FORCE: %.2f",
                tanks.get(1).fireAngle, tanks.get(1).firePower), 10, 45);

        BufferedImage mammuth = getTexture("mammuth");
        drawCenteredTexture(mammuth, B_WIDTH / 2, B_HEIGHT - mammuth.getHeight() / 2, 0);

        for (int i = 0; i < landscape.length; i++) {
            g.drawLine(i, landscape[i], i, B_HEIGHT);
        }

        g.drawLine(B_WIDTH - 50, 50,
                B_WIDTH - 50 + (int) (world.getWindForce() / 10 * Math.cos(Math.toRadians(world.getWindAngle()))),
                50 - (int) (world.getWindForce() / 10 * Math.sin(Math.toRadians(world.getWindAngle()))));

        for (Bullet bullet : bullets) {
            g.fillOval((int) bullet.getX() - 4, (int) bullet.getY() - 4, 8, 8);
        }

        g.setColor(Color.LIGHT_GRAY);

        for (Bullet bullet : leaves) {
            g.fillOval((int) bullet.getX(), (int) bullet.getY(), 2, 2);
        }

        for (Bullet bullet : particles) {
            g.fillOval((int) bullet.getX(), (int) bullet.getY(), 2, 2);
        }

        for (Tank tank : tanks) {
            //drawTank(g, tank);

            BufferedImage lufa = getTexture("lufa");
            drawCenteredTexture(lufa, tank.x, tank.y - 15, Math.toRadians(-tank.fireAngle));

            tank.draw(this);

//            g.setColor(Color.CYAN);
//
//            g.fillOval( (int) (tank.x + 30 * Math.cos(Math.toRadians(tank.fireAngle + 4))) - 2,
//                    (int) (tank.y - 30 * Math.sin(Math.toRadians(tank.fireAngle + 4))) - 15 - 2, 4 ,4);
//
//            g.fillOval((int) (tank.x ) - 2,
//                    (int) (tank.y ) - 15 - 2, 4, 4);
        }
    }

//    public void drawTank(Graphics2D g, Tank tank) {
//        g.setColor(tank.color);
//
//        g.setStroke(new BasicStroke(6));
//        g.drawLine((int) tank.x, (int) tank.y,
//                (int) (tank.x + 20 * Math.cos(Math.toRadians(tank.fireAngle))),
//                (int) (tank.y - 20 * Math.sin(Math.toRadians(tank.fireAngle))));
//
//        g.setStroke(new BasicStroke(8));
//        double dx = TANK_LEN / 2 * Math.cos(tank.rotation);
//        double dy = TANK_LEN / 2 * Math.sin(tank.rotation);
//        g.drawLine((int) (tank.x - dx), (int) (tank.y - dy), (int) (tank.x + dx), (int) (tank.y + dy));
//    }
}
