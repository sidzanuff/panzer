package com.circumsolar.panzer.game;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class World {

    private static final float DEFAULT_GRAVITY = 800;
    private static final float DEFAULT_WIND = 200;

    private float gravityForce = DEFAULT_GRAVITY;

    private float windAngle;
    private float windForce = DEFAULT_WIND;

    public Vector2D getGravity() {
        return new Vector2D(0, -gravityForce);
    }

    public Vector2D getWind() {
        return new Vector2D(
                Math.cos(Math.toRadians(windAngle)) * windForce,
                Math.sin(Math.toRadians(windAngle)) * windForce);
    }

    public float getGravityForce() {
        return gravityForce;
    }

    public void setGravityForce(float force) {
        gravityForce = force;
    }

    public float getWindAngle() {
        return windAngle;
    }

    public void setWindAngle(float windAngle) {
        this.windAngle = windAngle;
    }

    public float getWindForce() {
        return windForce;
    }

    public void setWindForce(float windForce) {
        this.windForce = windForce;
    }
}
