package com.circumsolar.panzer.game;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class Bullet {

    private World world;

    private Vector2D vel;
    private Vector2D acc;

    private float mass;

    private float x;
    private float y;

    public Bullet(World world, float mass, float startX, float startY, float angle, float power) {
        this.world = world;
        this.x = startX;
        this.y = startY;
        this.mass = mass;
        Vector2D force = new Vector2D(Math.cos(Math.toRadians(angle)) * power, Math.sin(Math.toRadians(angle)) * power);
        acc = force.scalarMultiply(1 / mass);
        vel = acc;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void update(float delta) {
        vel = vel.add(acc.scalarMultiply(delta));
        acc = acc.add(delta, world.getGravity()).add(delta / mass, world.getWind());
        x += vel.scalarMultiply(delta).getX();
        y -= vel.scalarMultiply(delta).getY();
    }
}
