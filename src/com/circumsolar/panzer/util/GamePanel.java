package com.circumsolar.panzer.util;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class GamePanel extends JPanel implements Runnable, KeyListener {

    private static final int DELAY = 10;

    private Map<Integer, Boolean> keyState = new HashMap<>();
    private Map<Integer, Boolean> keyRead = new HashMap<>();
    private Map<String, BufferedImage> textures = new HashMap<>();

    private Graphics2D currentGraphics;

    private final Object lock = new Object();

    public GamePanel(int width, int height) {
        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(width, height));
        setDoubleBuffered(true);
        addKeyListener(this);
        setFocusable(true);
    }

    @Override
    public void addNotify() {
        super.addNotify();

        Thread animator = new Thread(this);
        animator.start();
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        currentGraphics = g;

        synchronized (lock) {
            draw(g);
        }

        currentGraphics = null;

        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void run() {
        long last = System.currentTimeMillis();
        long timeDiff, sleep;

        //noinspection InfiniteLoopStatement
        while (true) {

            long elapsed = System.currentTimeMillis() - last;
            last = System.currentTimeMillis();

            synchronized (lock) {
                update(elapsed / 1000f);
            }

            repaint();

            timeDiff = System.currentTimeMillis() - last;
            sleep = DELAY - timeDiff;

            if (sleep < 0) {
                sleep = 2;
            }

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println("Interrupted: " + e.getMessage());
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        keyState.put(e.getKeyCode(), true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keyState.put(e.getKeyCode(), false);
        keyRead.put(e.getKeyCode(), false);
    }

    protected void loadTexture(String key) {
        try {
            BufferedImage texture = ImageIO.read(getClass().getResource("/" + key + ".png"));
            textures.put(key, texture);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected BufferedImage getTexture(String key) {
        return textures.get(key);
    }

    protected void drawCenteredTexture(BufferedImage texture, float x, float y, double rotation) {
        int width = texture.getWidth();
        int height = texture.getHeight();
        drawCenteredTexture(texture, x, y, rotation, width / 2, height / 2);
    }

    protected void drawCenteredTexture(BufferedImage texture, float x, float y, double rotation, int rotationX, int rotationY) {
        int width = texture.getWidth();
        int height = texture.getHeight();

        if (rotation != 0) {
            AffineTransform tx = AffineTransform.getRotateInstance(rotation, rotationX, rotationY);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
            texture = op.filter(texture, null);
        }

        currentGraphics.drawImage(texture,
                (int) (x - width / 2),
                (int) (y - height / 2),
                (int) (x + width / 2),
                (int) (y + height / 2),
                0, 0, width, height,
                null
        );
    }

    public boolean getKeyState(int key) {
        if (keyState.containsKey(key)) {
            return keyState.get(key);
        }
        return false;
    }

    public boolean getKeyStateOnce(int key) {
        boolean result = (keyState.containsKey(key) && keyState.get(key)) &&
                (!keyRead.containsKey(key) || (keyRead.containsKey(key) && !keyRead.get(key)));
        if (result) {
            keyRead.put(key, true);
        }
        return result;
    }

    protected abstract void update(float delta);

    protected abstract void draw(Graphics2D g);
}
