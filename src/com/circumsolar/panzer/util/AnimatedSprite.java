package com.circumsolar.panzer.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class AnimatedSprite {

    public float x;
    public float y;
    public int timePerFrame;
    public BufferedImage texture;
    public int width;
    public int height;
    public int textureColumns;
    public int frameCount;
    public float rotation;
    public float offsetX;
    public float offsetY;

    private int currentFrame;
    private float totalElapsed;
    private BufferedImage[] frames;

    public void prepare() {
        frames = new BufferedImage[frameCount];
        for (int i = 0; i < frameCount; i++) {
            BufferedImage frame = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics g = frame.getGraphics();
            int row = i / textureColumns;
            int column = i % textureColumns;
            g.drawImage(texture, 0, 0, width, height, width * column, height * row, width * column + width,
                    height * row + height, null);
            frames[i] = frame;
        }
    }

    public void update(float delta) {
        totalElapsed += delta * 1000;
        if (totalElapsed > timePerFrame) {
            currentFrame++;
            currentFrame = currentFrame % frameCount;
            totalElapsed -= timePerFrame;
        }
    }

    public void draw(GamePanel panel) {
        panel.drawCenteredTexture(frames[currentFrame], x + offsetX, y + offsetY, rotation);
    }
}
