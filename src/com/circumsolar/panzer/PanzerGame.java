package com.circumsolar.panzer;

import com.circumsolar.panzer.game.GameModel;

import javax.swing.*;
import java.awt.*;

public class PanzerGame extends JFrame {

    public PanzerGame() {
        initUI();
    }

    private void initUI() {

        add(new GameModel());

        setResizable(false);
        pack();

        setTitle("Panzer!");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame ex = new PanzerGame();
                ex.setVisible(true);
            }
        });
    }

}
